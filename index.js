const express = require('express');
const cors = require('cors');
const routerApi = require('./routes');

const {logErrors, errorHandler, boomErrorHandler} = require('./middlewares/error.handler');

const app = express();
// const port = process.env.PORT || 3000;
const port = 3000;

app.use(express.json());//recibir informacion por Post

const whitelist = ['http://localhost:8080', 'http://myapp.co'];
const options = {
   origin: (origin, callback) => {
      // if (whitelist.includes(origin) || !origin) {
      if (whitelist.includes(origin)) {
         callback(null, true);
      } else {
         callback(new Error('no permitido'));
      }
   }
}
app.use(cors(options));

app.get('/', (req, res) => {
   res.send('Hola mi server');
})

app.get('/nueva-ruta', (req, res) => {
   res.send('Hola nueva ruta');
});

routerApi(app);

app.use(logErrors);
app.use(boomErrorHandler);
app.use(errorHandler);


app.listen(port, () => {
   console.log('Mi port ' + port)
})
